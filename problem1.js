// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

// To find the information of a car with a given id.
function findInformation(carList,id) {
    
    if (carList == null || id == null || !(typeof id == "number") || !(Array.isArray(carList)) || carList.length == 0){
        return [];
    }else{
        for(let i = 0; i < carList.length ; i ++) {
            if(carList[i].id == id){
                return [carList[i]];
            }
        }
    }
}



module.exports = findInformation;
