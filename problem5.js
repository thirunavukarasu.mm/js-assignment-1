// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

// To find the number of cars older than 2000.
function olderCars(carList) {
    if (carList == null || !(Array.isArray(carList)) || carList.length == 0){
        return [];
    }else{
        let resultListWithYears = [];
        for( let i = 0; i < carList.length; i++){
            if(carList[i].car_year < 2000){
                resultListWithYears.push(carList[i].car_year);
            }
        }
        return resultListWithYears;
    }


}

module.exports = olderCars;
