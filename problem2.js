// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
//"Last car is a *car make goes here* *car model goes here*"


// To find the information of the last car.
function findInformationOfLastCar(carList) {
    if (carList == null || !(Array.isArray(carList)) || carList.length == 0){
        return [];
    }else{
        let index = carList.length - 1
        return [carList[index]];
    }
            
}



module.exports = findInformationOfLastCar;
