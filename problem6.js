// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


// To get only the BMW and Audi cars.
function BMWAndAudiFilter(carList) {
    if (carList == null || !(Array.isArray(carList)) || carList.length == 0){
        return [];
    }else{
        let BMWAndAudi = [];
        for( let i = 0; i < carList.length; i++){
            if(carList[i].car_make == "Audi" || carList[i].car_make == "BMW"){
                BMWAndAudi.push(carList[i]);
            }
            
        }
        return BMWAndAudi;
    }
}


module.exports = BMWAndAudiFilter;