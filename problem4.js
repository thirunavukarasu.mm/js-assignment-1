// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

// To get all the years in the array.
function getYearsOfEveryCars(carList) {
    if (carList == null || !(Array.isArray(carList)) || carList.length == 0){
        return [];
    }else{
        let yearList = [];
        for( let i = 0; i < carList.length; i++){
            yearList.push(carList[i].car_year);
        }
        return yearList;
    }
}



module.exports = getYearsOfEveryCars;