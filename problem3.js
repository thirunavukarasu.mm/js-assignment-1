// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


// To sort the array alphabatically based on the car_model.
function carModelSorted(carList) {
    if (carList == null || !(Array.isArray(carList)) || carList.length == 0){
        return [];
    }else{
        return carList.sort((a, b) => a.car_model.localeCompare(b.car_model));
    }
            
}



module.exports = carModelSorted;
